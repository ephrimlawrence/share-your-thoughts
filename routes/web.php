<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::post('/message/send', 'HomeController@sendMessage')->name('send-message');

Route::get('/admin', 'AdminController@index')->middleware('is_admin')->name('admin-index');

Route::get('/admin/export/download', 'AdminController@export')->middleware('is_admin')->name('download-user-data');

Route::get('/admin/export/remote', 'AdminController@remoteExport')->middleware('is_admin')->name('remote-export-user-data');
