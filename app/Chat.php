<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model {
    public $table = 'chats';

    public $fillable = ['message', 'added_by'];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
