# Share Your Thouths
> A simple chat application built with laravel

![Screenshot](./screenshot.png)

## Requirements
* [PHP >= 7.1.3](https://php.net)
* [Laravel PHP Web Framework](https://laravel.com/)
* [Composer](https://getcomposer.org/)
* PHP

## Installation
1. Install [Laravel PHP Web Framework](https://laravel.com/)
2. Clone or [download the  zip](https://gitlab.com/ephrimlawrence/share-your-thoughts/-/archive/master/share-your-thoughts-master.zip) of the project
	```bash
	git clone https://gitlab.com/ephrimlawrence/share-your-thoughts.git
	```
3. Change directory to project folder and install project dependencies
  ```bash
  cd /path/to/project
  
  # make sure composer is in your system path
  composer install
  
  touch database/database.sqlite # create the database
  
  php artisan migrate # make database migration
  ```
4. Run the server
	```bash
	php artisan serve -vv
	```
	> Open your browser to [localhost:8000](http://localhost:8000), create new account and start chatting.


## Administrator Account
![Admin Dashboard](./screenshot-admn.png)

### Creating admin account
To create admin account, execute the following command
```bash
php artisan register:admin
```
> Visit [localhost:8000/admin](http://localhost:8000/admin) and login as admin

### Downloading user account details
To download user account details, click on the [Download User Data](http://localhost:8000/admin/export/download) button

### Downloading user acount details to remote server
Before you can export to a remote computer, set the following configurations in your terminal:
```bash
export FTP_HOST="REMOTE_SERVER_IP"
export FTP_USERNAME="REMOTE_SERVER_FTP_USERNAME" # defaults to 'anonymous' user
export FTP_PASSWORD="REMOTE_SERVER_FTP_PASSWORD" # leave blank if FTP_USERNAME is blank

# to make the config persistent, add it to your bash_rc config
```
Visit [http://localhost:8000/admin/export/remote](http://localhost:8000/admin/export/remote) to export user details to your remote server.

**NOTE** Due to [vsftpd](https://wiki.archlinux.org/index.php/Very_Secure_FTP_Daemon) permission errors I had even using __bash__, I couldn't create a folder using this feature.
This feature may or may not work depending on your FTP server configuration.
