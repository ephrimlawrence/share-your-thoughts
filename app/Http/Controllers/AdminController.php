<?php

namespace App\Http\Controllers;

use App\User;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('admin', ['users'=>User::where('type', '=', 'default')->oldest()->get()]);
    }

    public function export() {
        $filename = 'user-details-' . date('d-m-Y-H:i:s') . '.csv';
        $headers = [
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=$filename',
            'Pragma'              => 'no-cache',
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Expires'             => '0'
        ];

        $users = DB::table('users')->select('id', 'name', 'email', 'created_at', 'updated_at')
            ->where('type', '=', 'default')
            ->orderBy('id', 'asc')->get();
        $columns = ['id', 'name', 'email', 'created_at', 'updated_at'];

        $callback = function () use ($users, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($users as $user) {
                fputcsv($file, [$user->id, $user->name, $user->email, $user->created_at, $user->updated_at]);
            }
            fclose($file);
        };

        return response()->streamDownload($callback, $filename, $headers);
    }

    public function remoteExport() {
        $filename = 'user-details-' . date('d-m-Y-H:i:s') . '.csv';
        $headers = [
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=$filename',
            'Pragma'              => 'no-cache',
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Expires'             => '0'
        ];

        $users = DB::table('users')->select('id', 'name', 'email', 'created_at', 'updated_at')
            ->where('type', '=', 'default')
            ->orderBy('id', 'asc')->get();
        $columns = ['id', 'name', 'email', 'created_at', 'updated_at'];

        $file = fopen('user-data.csv', 'w');
        fputcsv($file, $columns);

        foreach ($users as $user) {
            fputcsv($file, [$user->id, $user->name, $user->email, $user->created_at, $user->updated_at]);
        }
        fclose($file);
        Storage::disk('ftp')->put($filename, fopen('user-data.csv', 'r+'));

        return back()->with('success', 'File uploaded successfully');
    }
}
