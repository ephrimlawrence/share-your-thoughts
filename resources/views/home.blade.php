@extends('layouts.app')

@section('content')
<div class="container" id="main">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Chats</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="chats">
                        @foreach ($messages as $msg)
                            <div class="d-flex pt-2 {{ $msg->added_by == Auth::id() ? 'flex-row-reverse' : ''}}">
                                <div class="card w-75">
                                    <div class="card-body">
                                        {{ $msg->message }}
                                        <div class="text-muted pt-1">
                                            {{ Carbon\Carbon::parse($msg->created_at)->diffForHumans() }} by: {{ $msg->name }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <form action="{{ route('send-message') }}" method="post">
                        @csrf

                        <div class="input-group mb-3 mt-3">
                            <textarea class="form-control" placeholder="Enter your message" aria-label="Enter your message" aria-describedby="send-message" name="message" rows="1" cols="1" required></textarea>

                            <div class="input-group-append">
                                <input class="btn btn-outline-secondary" type="submit" id="send-message" value="Send Message">
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        $('#main').scrollTop($('#main')[0].scrollHeight);
    });
</script>
@endsection
