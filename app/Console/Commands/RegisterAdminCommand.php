<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class RegisterAdminCommand extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'register:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register Admin';

    /**
     * User model.
     *
     * @var object
     */
    private $user;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(User $user) {
        parent::__construct();

        $this->user = $user;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $details = $this->getDetails();
        $admin = $this->user->createAdmin($details);
        $this->display($admin);
    }

    /**
     * Ask for admin details.
     *
     * @return array
     */
    private function getDetails() : array {
        $details['name'] = $this->ask('Name');
        $details['email'] = $this->ask('Email');
        $details['password'] = $this->secret('Password');
        $details['confirm_password'] = $this->secret('Confirm password');
        while (!$this->isValidPassword($details['password'], $details['confirm_password'])) {
            if (strlen($details['password']) <= 6) {
                $this->error('Password must be more that six characters');
            }
            if (!$this->isMatch($details['password'], $details['confirm_password'])) {
                $this->error('Password and Confirm password do not match');
            }
            $details['password'] = $this->secret('Password');
            $details['confirm_password'] = $this->secret('Confirm password');
        }

        $details['password'] = Hash::make($details['password']);

        return $details;
    }

    /**
    * Display created admin.
    *
    * @param array $admin
    * @return void
    */
    private function display(User $admin) : void {
        $headers = ['Name', 'Email', 'Admin'];
        $fields = [
            'Name'  => $admin->name,
            'email' => $admin->email,
            'admin' => $admin->isAdmin()
        ];
        $this->info('Admin account created');
        $this->table($headers, [$fields]);
    }

    /**
    * Check if password is vailid
    *
    * @param string $password
    * @param string $confirmPassword
    * @return boolean
    */
    private function isValidPassword(string $password, string $confirmPassword) : bool {
        return (strlen($password) > 6) &&
        $this->isMatch($password, $confirmPassword);
    }

    /**
     * Check if password and confirm password matches.
     *
     * @param string $password
     * @param string $confirmPassword
     * @return bool
     */
    private function isMatch(string $password, string $confirmPassword) : bool {
        return $password === $confirmPassword;
    }
}
