<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chat;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $result = DB::table('chats')
            ->join('users', 'chats.added_by', '=', 'users.id')
            ->select('users.name', 'chats.*')
            ->get();

        return view('home', ['messages'=>$result]);
    }

    public function sendMessage(Request $request) {
        Chat::create([
            'message' => $request->input('message'),
            'added_by'=> auth()->user()->id
        ]);

        return back();
    }
}
